<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Hewan: $sheep->nama<br>";
echo "Kaki: $sheep->kaki<br>";
echo "Darah dingin: $sheep->cold<br> <br>";

$kodok = new frog("Kodok");
echo "Hewan: $kodok->nama<br>";
echo "kaki: $kodok->kaki<br>";
echo "Darah dingin: $kodok->cold<br>";
echo $kodok->Lompat();

$kera = new ape("Kera");
echo "<br><br>Hewan: $kera->nama<br>";
echo "kaki: $kera->kaki<br>";
echo "Darah dingin: $kera->cold<br>";
echo $kera->yell();
?>